﻿using SimpleWebScraper.Builders;
using SimpleWebScraper.Data;
using SimpleWebScraper.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleWebScraper
{
    class Program
    {
        private const string Method = "search";
        private const string Currency = "AUD";
        private const string Format = "Vinyl";

        private const string tvtype = "LED-TVs";
        private const int buyitnow = 1;
        private const string products = "digital-slr-cameras";

        public static IWebProxy Proxy { get; private set; }

        static void Main(string[] args)
        {

            try
            {
                Console.Clear();
                Console.WriteLine("\nSimple Web Scraper.");
                Console.WriteLine("===================");
                //Console.WriteLine("\nPlease enter city name: ");
                //Console.WriteLine("\nPlease enter genre: ");
                Console.WriteLine("\nPlease enter camera brand: ");
                //var genre = Console.ReadLine() ?? string.Empty;
                //var cityName = Console.ReadLine() ?? string.Empty;
                var brand = Console.ReadLine() ?? string.Empty;


                //Console.WriteLine();
                //Console.WriteLine("\nPlease enter year: ");
                //var year = Console.ReadLine() ?? string.Empty;

                Console.WriteLine();
                Console.WriteLine("\nPlease enter condition: ");
                var condition = Console.ReadLine() ?? string.Empty;

                //Console.WriteLine();
                //Console.WriteLine("\nPlease enter category: ");
                //var category = Console.ReadLine() ?? string.Empty;

                Console.WriteLine();


                IWebProxy defaultWebProxy = WebRequest.DefaultWebProxy;
                defaultWebProxy.Credentials = CredentialCache.DefaultNetworkCredentials;

                
                using (WebClient client = new WebClient())
                {
                    Proxy = defaultWebProxy;

                    string content = client.DownloadString($"https://www.cameracorp.com.au/{products}/{condition}/{brand}/");

                    ScrapeCriteria scrapeCriteria = new ScrapeCriteriaBuilder()
                        .WithData(content)
                        .WithRegex(@"<div class=\""details\""><h5><a href=\""(.*?)\""><span class=\""(.*?)\"">(.*?)</span>")
                        .WithRegexOption(RegexOptions.ExplicitCapture)
                        .WithPart(new ScrapeCriteriaPartBuilder()
                            .WithRegex(@"<span class=\""visible-xs condition preloved\"">(.*?)</span>")
                            .WithRegexOption(RegexOptions.Singleline)
                            .Build())
                        .WithPart(new ScrapeCriteriaPartBuilder()
                            .WithRegex(@"<div class=\""details\""><h5><a href=\""(.*?)\"">")
                            .WithRegexOption(RegexOptions.Singleline)
                            .Build())
                        .Build();

                    Scraper scraper = new Scraper();

                    var scrapedElements = scraper.Scrape(scrapeCriteria);

                    if (scrapedElements.Any())
                    {
                        foreach (var scrapedElement in scrapedElements)
                        {
                            Console.WriteLine(scrapedElement);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No Matches Found!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception " + ex.Message);
            }

            Console.Write("\nPress enter to continue!");
            Console.ReadKey();

        }
    }
}
